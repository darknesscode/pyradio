# Pyradio

PyRadio is a console based Internet radio player. It is implemented in Python, and uses mplayer for media playback

![pyradio - darknesscode](/config-files/pyradio.png)

# What this does

* Arch Linux, Debian and Void Linux Systems
  * Install requirements
  * Install pyradio from sourse (included in this repo)
  * Copy new config files
* Edit stations file

### Note

PyRadio Version 0.8.9.9

PyRadio now can be update from the terminal (need python-requests installed)

```
pyradio -U
```

Python3-setuptools is a dependency for PyRadio.
